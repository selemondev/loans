/* eslint-disable prettier/prettier */
import { createRouter, createWebHistory } from 'vue-router';
import AppLayout from '@/layout/AppLayout.vue';
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'login',
            component: () => import('../views/pages/auth/LoginView.vue')
        },
        {
            path: '/',
            component: AppLayout,
            children: [
                {
                    path: '/home',
                    name: 'dashboard',
                    component: () => import('../views/Dashboard.vue'),
                    meta: {
                        sidebar: true,
                        auth: true
                    }
                },

                {
                    path: "/dashboard/loan-type",
                    name: "loan-type",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/dashboard/LoanType.vue")
                },
                {
                    path: "/dashboard/branch",
                    name: "branches",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/dashboard/BranchesView.vue")
                },
                {
                    path: "/dashboard/field",
                    name: "field",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/dashboard/FieldView.vue")

                },
                {
                    path: "/dashboard/user",
                    name: "user-management",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/dashboard/UserManagement.vue")
                },
                {
                    path: "/dashboard/sections",
                    name: "dashboard-sections",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/dashboard/SectionsView.vue"),
                },
                {
                    path: "/dashboard/sections/fields/:fieldId",
                    name: "dashboard-sections-fields",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/dashboard/FieldView.vue")
                },
                {
                    path: "/dashboard/officer/loans",
                    name: "officer-loans",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/relationShipOfficerDashboard/Loans.vue")
                },
                {
                    path: "/dashboard/officer/loans/:id",
                    name: "officer-loans-id",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/relationShipOfficerDashboard/SingleLoan.vue")
                },
                {
                    path: "/dashboard/credit/loans",
                    name: "credit-loans",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/creditAnalystDashboard/creditView.vue")
                },
                {
                    path: "/dashboard/credit/loans/:id",
                    name: "credit-loans-id",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/creditAnalystDashboard/SingleLoan.vue")
                },
                {
                    path: "/dashboard/approver/loans",
                    name: "approver-loans",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/approverDashboard/ApproverView.vue")
                },
                {
                    path: "/dashboard/approver/loans/:id",
                    name: "approver-loans-id",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/approverDashboard/SingleApprover.vue")
                },

                {
                    path: "/dashboard/credit-admin/loans",
                    name: "credit-admin-loans",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/creditAdminDashboard/creditAdminDashboard.vue")
                },
                {
                    path: "/dashboard/credit-admin/loans/:id",
                    name: "credit-admin-loans-id",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/creditAdminDashboard/singleCreditAdmin.vue")
                },

                {
                    path: "/dashboard/branch-manager/loans",
                    name: "branch-manager-loans",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/branchManagerDashboard/branchManagerDashboard.vue")
                },
                {
                    path: "/dashboard/branch-manager/loans/:id",
                    name: "branch-manager-loans-id",
                    meta: { sidebar: true, auth: true },
                    component: () => import("../views/branchManagerDashboard/singleBranchManager.vue")
                }
            ]
        },
        {
            path: '/auth/recovery',
            name: 'recovery',
            component: () => import('../views/pages/auth/RecoveryView.vue')
        },
        {
            path: '/auth/reset/:token',
            name: 'reset',
            component: () => import('../views/pages/auth/ResetPasswordView.vue')
        },
        {
            path: "/registration-success",
            name: "registration-success",
            component: () => import('../views/pages/auth/RegistrationSuccess.vue')
        }
    ]
});


const currentUser = () => {
    return new Promise((resolve, reject) => {
        const token = JSON.parse(localStorage.getItem("accessToken"));
        resolve(token);
        reject;
    });
};

router.beforeEach(async (to, from, next) => {
    if (to.matched.some((record) => record.meta.auth === true)) {
        if (await currentUser()) {
            next();
        } else {
            next({
                path: "/",
                //   query: { redirect: to.fullPath }
            });
        }
    } else {
        next();
    }
});

export default router;
