import millify from "millify";
export const useMillify = (val) => {
    return millify(val);
}