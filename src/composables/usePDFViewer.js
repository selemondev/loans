import { ref, onMounted } from 'vue';
import pdfjsLib from 'pdfjs-dist';

export default function usePDFViewer(url) {
  const pdfViewer = ref(null);

  onMounted(async () => {
    const pdf = await pdfjsLib.getDocument(url).promise;
    const page = await pdf.getPage(1);
    const canvas = pdfViewer.value;
    const context = canvas.getContext('2d');

    const viewport = page.getViewport({ scale: 1 });
    canvas.width = viewport.width;
    canvas.height = viewport.height;

    const renderContext = {
      canvasContext: context,
      viewport: viewport,
    };

    await page.render(renderContext);
  });

  return { pdfViewer };
}
