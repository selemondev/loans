import { axiosPrivate } from "../api/axios";
export default {
    install: (app) => {
        app.config.globalProperties.$http = axiosPrivate;
        const $http = app.config.globalProperties.$http;
        $http.interceptors.request.use(
            function (config) {
                return config;
            },
            function (error) {
                if (error.isAxiosError && !error.response) {
                    // Handle network error
                    console.log(error.message)
                }
                return Promise.reject(error);
            }
        );

        $http.interceptors.response.use(
            function (response) {
                return response;
            },
            function (error) {
                if (error.response && error.response.status === 401) {
                    // Handle unauthorized error
                    window.location.href = "/";
                    localStorage.removeItem("accessToken");
                    localStorage.removeItem("userData");
                    localStorage.removeItem('role')
                }

                if (error.response && error.response.status === 500) {
                    // Handle unauthorized error
                    console.log(error.message)
                }
                return Promise.reject(error);
            }

        );
        app.provide('fetch', $http)
    }
}