import axios from "axios";
const accessToken = JSON.parse(localStorage.getItem("accessToken")) ? JSON.parse(localStorage.getItem("accessToken")) : '';
const baseUrl = import.meta.env.VITE_API_KEY;
export default axios.create({
    baseURL: baseUrl
});

export const axiosPrivate = axios.create({
    baseURL: baseUrl,
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Origin': '*',
        'Authorization': `Bearer ${accessToken}`
    },
    timeout: 60 * 1000
});